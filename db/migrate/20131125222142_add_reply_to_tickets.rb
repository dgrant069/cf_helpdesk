class AddReplyToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :answer, :string
  end
end
