require 'test_helper'

class TicketMailerTest < ActionMailer::TestCase
  test "help_request" do
    mail = TicketMailer.help_request
    assert_equal "Help request", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
