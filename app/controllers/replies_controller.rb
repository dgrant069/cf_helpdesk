class RepliesController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :create

  def index
    @replies = Reply.all
  end

  def new
    @reply = Reply.new
  end

  def create
    # @reply_params = JSON.parse(params[:ticket_id => "Subject".gsub("Re: Help desk ticket has been created, ID ", "").to_i, :reply => "TextBody"])
    # @reply = Reply.new.create_from_inbound_hook(Postmark::Json.decode(request.body.read))
    @subject = params["Subject"].gsub("Re: Help desk ticket has been created, ID ", "").to_i
    @body = params["TextBody"]
    @reply = Reply.new(:ticket_id => @subject, :reply => @body)

    @reply.save
    render nothing: true
  end
end
