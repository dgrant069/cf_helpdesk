class TicketsController < ApplicationController
  # skip_before_filter :verify_authenticity_token, only: :inbound_ticket_reply
  # before_filter :authenticate_postmark, only: [:create]

  def index
    @tickets = Ticket.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tickets }
    end
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @ticket = Ticket.find(params[:id])
    @replies = Reply.all

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ticket }
    end
  end

  # GET /tickets/new
  # GET /tickets/new.json
  def new
    @ticket = Ticket.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ticket }
    end
  end

  # GET /tickets/1/edit
  def edit
    @ticket = Ticket.find(params[:id])
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(params[:ticket])

    respond_to do |format|
      if @ticket.save
        TicketMailer.help_request(@ticket).deliver
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render json: @ticket, status: :created, location: @ticket }
      else
        format.html { render action: "new" }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # def inbound_ticket_reply
  #   @ticket = Ticket.update_from_inbound_hook(Postmark::Json.decode(request.body.read))

  #   respond_to do |format|
  #     if @ticket.save
  #       format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
  #       format.json { render json: @ticket, status: :created, location: @ticket }
  #     else
  #       format.html { render action: "new" }
  #       format.json { render json: @ticket.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end

  # PUT /tickets/1
  # PUT /tickets/1.json
  def update
    @ticket_id = Ticket.find(params[:id])
    # @ticket = Ticket.update_from_inbound_hook(Postmark::Json.decode(request.body.read))

    respond_to do |format|
      if @ticket_id.update_attributes(params[:ticket])
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket = Ticket.find(params[:id])
    @ticket.destroy

    respond_to do |format|
      format.html { redirect_to tickets_url }
      format.json { head :no_content }
    end
  end
end
