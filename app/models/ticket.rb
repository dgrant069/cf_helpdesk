class Ticket < ActiveRecord::Base
  attr_accessible :problem, :title, :name, :email, :reply
  validates_format_of :email, :with => /@/
  has_many :replies

#   def self.update_from_inbound_hook(message)
#     self.update(message["Subject"].gsub("Re: Help desk ticket has been created, ID ", ""), :reply => message["TextBody"])
#   end
end

# Try creating a new model and db for replies and then attr access in the controller (model?) and list in view
