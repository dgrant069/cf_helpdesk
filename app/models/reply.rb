class Reply < ActiveRecord::Base
  attr_accessible :reply, :ticket_id
  belongs_to :ticket

  # def self.create_from_inbound_hook(message)
  #   self.create(:ticket_id => message["Subject"].gsub("Re: Help desk ticket has been created, ID ", "").to_i, :reply => message["TextBody"])
  # end
end
