class TicketMailer < ActionMailer::Base
  default from: "info@snappacker.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.ticket_mailer.help_request.subject
  #
  def help_request(ticket)
    @ticket = ticket
    mail to: "dev.test.scenario@gmail.com", subject: "Help desk ticket has been created, ID #{@ticket.id}"
  end

  # def help_incoming_reply(email)
  #   @ticket.inbound_ticket_reply(
  #     :reply => email.body
  #   )
  # end
end
